# curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
#    "$class": "subsidytracker.network.ProcCentre", 
#    "procCentreId": "pc1", 
#    "address": "Procurement Cetre Address 1", 
#    "officerName": "Office name 1", 
#    "officerId": "AB", 
#    "warehouseIds": ["wh1", "wh2"] 
#  }' 'http://localhost:3000/api/ProcCentre'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.ProcCentre", 
   "procCentreId": "pc1", 
   "address": "Procurement Centre Address 1", 
   "officerName": "Office name 1", 
   "officerId": "AB", 
   "shopIds": ["sh1", "sh2"]
 }' 'http://localhost:3000/api/ProcCentre'

#  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
#    "$class": "subsidytracker.network.Warehouse", 
#    "warehouseId": "wh1", 
#    "address": "Warehouse Address 1", 
#    "managerName": "Manager Name 1", 
#    "managerId": "PD1", 
#    "shopIds": ["sh1", "sh2"] 
#  }' 'http://localhost:3000/api/Warehouse'
#  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
#    "$class": "subsidytracker.network.Warehouse", 
#    "warehouseId": "wh2", 
#    "address": "Warehouse Address 2", 
#    "managerName": "Manager Name 2", 
#    "managerId": "KK1", 
#    "shopIds": ["sh2"] 
#  }' 'http://localhost:3000/api/Warehouse'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.Shop", 
   "shopId": "sh1", 
   "address": "Shop Address 1", 
   "ownerName": "Owner Name 1", 
   "ownerId": "PP1", 
   "phoneNo": "120", 
   "licenseNo": "LS1" 
 }' 'http://localhost:3000/api/Shop'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.Shop", 
   "shopId": "sh2", 
   "address": "Shop Address 2", 
   "ownerName": "Owner Name 2", 
   "ownerId": "DD1", 
   "phoneNo": "129",
   "licenseNo": "LS2" 
 }' 'http://localhost:3000/api/Shop'

 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs1", 
   "quantity": "20", 
   "itemName": "rice", 
   "procCentreId": "pc1", 
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs2", 
   "quantity": "20", 
   "itemName": "rice", 
   "procCentreId": "pc1", 
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs3", 
   "quantity": "20", 
   "itemName": "Rice", 
   "procCentreId": "pc1",  
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs4", 
   "quantity": "100", 
   "itemName": "Olive Oil", 
   "procCentreId": "pc1", 
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs5", 
   "quantity": "20", 
   "itemName": "Rice", 
   "procCentreId": "pc1",  
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs6", 
   "quantity": "20", 
   "itemName": "Rice", 
   "procCentreId": "pc1", 
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs7", 
   "quantity": "20", 
   "itemName": "Wheat", 
   "procCentreId": "pc1",  
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.FoodSack", 
   "foodSackId": "fs8", 
   "quantity": "20", 
   "itemName": "Wheat", 
   "procCentreId": "pc1", 
   "shopId": " ", 
   "qualityStatus": "Accepted" 
 }' 'http://localhost:3000/api/FoodSack'



 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.Citizen", 
   "citizenId": "a1", 
   "dob": "589836", 
   "firstName": "FirstName1", 
   "middleName": "MiddleName1", 
   "lastName": "LastName1", 
   "phoneNo": "123", 
   "quantity": "{ \"Rice\": 30, \"Wheat\": 40, \"Olive Oil\": 2  }", 
   "invokedInMonth": "{ \"Rice\": 0, \"Wheat\": 0, \"Olive Oil\": 0  }"
 }' 'http://localhost:3000/api/Citizen'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.Citizen", 
   "citizenId": "a2", 
   "dob": "5895876", 
   "firstName": "FirstName2", 
   "middleName": "MiddleName2", 
   "lastName": "LastName2", 
   "phoneNo": "124", 
   "quantity": "{ \"Rice\": 30, \"Wheat\": 40, \"Olive Oil\": 2  }", 
   "invokedInMonth": "{ \"Rice\": 0, \"Wheat\": 0, \"Olive Oil\": 0  }"
 }' 'http://localhost:3000/api/Citizen'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.Citizen", 
   "citizenId": "a3", 
   "dob": "589836", 
   "firstName": "FirstName3", 
   "middleName": "MiddleName3", 
   "lastName": "LastName3", 
   "phoneNo": "125", 
   "quantity": "{ \"Rice\": 30, \"Wheat\": 40, \"Olive Oil\": 2  }", 
   "invokedInMonth": "{ \"Rice\": 0, \"Wheat\": 0, \"Olive Oil\": 0  }"
 }' 'http://localhost:3000/api/Citizen'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.Citizen", 
   "citizenId": "a4", 
   "dob": "5895876", 
   "firstName": "FirstName4", 
   "middleName": "MiddleName4", 
   "lastName": "LastName4", 
   "phoneNo": "126", 
   "quantity": "{ \"Rice\": 30, \"Wheat\": 40, \"Olive Oil\": 2  }", 
   "invokedInMonth": "{ \"Rice\": 0, \"Wheat\": 0, \"Olive Oil\": 0  }"
 }' 'http://localhost:3000/api/Citizen'

#  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
#    "$class": "subsidytracker.network.AddToWarehouse", 
#    "warehouseId": "wh1", 
#    "sackId": "fs1", 
#    "transactionId": "string", 
#    "timestamp": "2019-06-26T17:37:29.495Z" 
#  }' 'http://localhost:3000/api/AddToWarehouse'

 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh1", 
   "sackId": "fs1",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh2", 
   "sackId": "fs2",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh1", 
   "sackId": "fs3",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh2", 
   "sackId": "fs3",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh1", 
   "sackId": "fs5",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh2", 
   "sackId": "fs6",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.AddToShop",
   "shopId": "sh1", 
   "sackId": "fs7",  
   "transactionId": "string", 
   "timestamp": "2019-06-26T17:37:29.479Z" 
 }' 'http://localhost:3000/api/AddToShop'


 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a1", 
   "shopId": "sh1", 
   "invokedQuantity": "10", 
   "sackId": "fs1", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a2", 
   "shopId": "sh1", 
   "invokedQuantity": "10", 
   "sackId": "fs1", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a1", 
   "shopId": "sh1", 
   "invokedQuantity": "15", 
   "sackId": "fs7", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a2", 
   "shopId": "sh2", 
   "invokedQuantity": "10", 
   "sackId": "fs8", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a3", 
   "shopId": "sh2", 
   "invokedQuantity": "20", 
   "sackId": "fs8", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a3", 
   "shopId": "sh2", 
   "invokedQuantity": "2", 
   "sackId": "fs4", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.InvokeSubsidy", 
   "citizenId": "a4", 
   "shopId": "sh2", 
   "invokedQuantity": "2", 
   "sackId": "fs4", 
   "transactionId": "string", 
   "timestamp": "2019-06-26T18:06:10.726Z" 
 }' 'http://localhost:3000/api/InvokeSubsidy'



 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{
   "$class": "subsidytracker.network.QCAuthority", 
   "QCId": "qc1", 
   "headName": "QC Name 1", 
   "headId": "RP1" 
 }' 'http://localhost:3000/api/QCAuthority'
 curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.CheckQuality", 
   "location": "ProcCentre", 
   "locationId": "pc1", 
   "QCId": "qc1", 
   "sackId": "fs1", 
   "status": "Accepted", 
   "timestamp": "2019-06-26T18:23:02.497Z" 
 }' 'http://localhost:3000/api/CheckQuality'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.CheckQuality", 
   "location": "ProcCentre", 
   "locationId": "pc1", 
   "QCId": "qc1", 
   "sackId": "fs3", 
   "status": "Accepted", 
   "timestamp": "2019-06-26T18:23:02.497Z" 
 }' 'http://localhost:3000/api/CheckQuality'
   curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.CheckQuality", 
   "location": "ProcCentre", 
   "locationId": "pc1", 
   "QCId": "qc1", 
   "sackId": "fs4", 
   "status": "Accepted", 
   "timestamp": "2019-06-26T18:23:02.497Z" 
 }' 'http://localhost:3000/api/CheckQuality'
   curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.CheckQuality", 
   "location": "ProcCentre", 
   "locationId": "pc1", 
   "QCId": "qc1", 
   "sackId": "fs6", 
   "status": "Accepted", 
   "timestamp": "2019-06-26T18:23:02.497Z" 
 }' 'http://localhost:3000/api/CheckQuality'
   curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.CheckQuality", 
   "location": "ProcCentre", 
   "locationId": "pc1", 
   "QCId": "qc1", 
   "sackId": "fs7", 
   "status": "Accepted", 
   "timestamp": "2019-06-26T18:23:02.497Z" 
 }' 'http://localhost:3000/api/CheckQuality'
  curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ 
   "$class": "subsidytracker.network.CheckQuality", 
   "location": "Shop", 
   "locationId": "sh1", 
   "QCId": "qc1", 
   "sackId": "fs5", 
   "status": "Rejected", 
   "timestamp": "2019-06-26T18:23:02.497Z" 
 }' 'http://localhost:3000/api/CheckQuality'

