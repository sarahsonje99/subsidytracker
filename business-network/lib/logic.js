'use strict';
/**
 * Write your transction processor functions here
 */

// 2. Emit 3 Events
// 3. permissions.acl check

// /**
//  * @param {subsidytracker.network.AddToWarehouse} AddToWarehouse
//  * @returns{String}
//  * @transaction
//  */
// async function AddToWarehouse(tx) {
//     const sackRegistry = await getAssetRegistry('subsidytracker.network.FoodSack');
//     const sack = await sackRegistry.get(tx.sackId);
//     let output = {};
//     if (sack.qualityStatus=="Rejected") {
//         output["status"] = "Rejected due to bad quality";
//         return JSON.stringify(output);
//     }
//     sack.warehouseId = tx.warehouseId;
//     await sackRegistry.update(sack);
//     output["status"] = "Successful";
//     return JSON.stringify(output);
// }

/**
 * @param {subsidytracker.network.AddToShop} AddToShop
 * @returns{String}
 * @transaction
 */
async function AddToShop(tx) {
    const sackRegistry = await getAssetRegistry('subsidytracker.network.FoodSack');
    const sack = await sackRegistry.get(tx.sackId);
    let output = {};
    if (sack.qualityStatus=="Rejected") {
        output["status"] = "Rejected due to bad quality";
        return JSON.stringify(output);
    }
    sack.shopId = tx.shopId;
    await sackRegistry.update(sack);
    output["status"] = "Successful";
    return JSON.stringify(output);
}

/**
 * @param {subsidytracker.network.CheckQuality} CheckQuality
 * @transaction
 */
async function CheckQuality(tx) {
    const sackRegistry = await getAssetRegistry('subsidytracker.network.FoodSack');
    const sack = await sackRegistry.get(tx.sackId);
    //location shld be Warehouse, ProcCentre or Shop
    let oldStatus = sack.qualityStatus;
    sack.qualityStatus = tx.status;
    await sackRegistry.update(sack);

    //emit an event "QualityChecked" with params
    let event = getFactory().newEvent('subsidytracker.network', 'QualityChecked');
    event.location = tx.location;
    event.locationId = tx.locationId;
    event.QCId = tx.QCId;
    event.sackId = tx.sackId;
    event.newStatus = tx.status;
    event.oldStatus = oldStatus;
    emit(event);
}

/**
 * @param {subsidytracker.network.InvokeSubsidy} InvokeSubsidy
 * @returns{String}
 * @transaction
 */
async function InvokeSubsidy(tx) {
    const sackRegistry = await getAssetRegistry('subsidytracker.network.FoodSack');
    const sack = await sackRegistry.get(tx.sackId);
    let itemType = sack.itemName
    let output = {};
    if (sack.qualityStatus=="Rejected") {
        output["status"] = "Rejected due to bad quality";
        return JSON.stringify(output);
    }
    const citizenRegistry = await getParticipantRegistry('subsidytracker.network.Citizen');
    const citizen = await citizenRegistry.get(tx.citizenId);
    pastInvoked = JSON.parse(citizen.invokedInMonth)
    if (tx.invokedQuantity > JSON.parse(citizen.quantity)[itemType]-pastInvoked[itemType]) {
        output["status"] = "Exceeds allocated quantity";
        output["balance"] = JSON.parse(citizen.quantity)[itemType]-pastInvoked[itemType];
        return JSON.stringify(output);
    }
    pastInvoked[itemType] = pastInvoked[itemType] + tx.invokedQuantity;
    citizen.invokedInMonth = JSON.stringify(pastInvoked)
    await citizenRegistry.update(citizen);
    const date = new Date();
    const timestamp = date.getTime();
    subsidyId = timestamp.toString() + "#" + tx.citizenId + "#" + tx.shopId
    await citizenRegistry.update(citizen);
    output["status"] = "Successful";
    output["subsidyId"] = subsidyId
    output["sackId"] = tx.sackId

    //emit an event "SubsidyInvoked" with params
    let event = getFactory().newEvent('subsidytracker.network', 'SubsidyInvoked');
    event.subsidyId = subsidyId;
    event.citizenId = tx.citizenId
    event.shopId = tx.shopId
    event.invokedQuantity = tx.invokedQuantity
    event.sackId = tx.sackId;
    emit(event);

    return JSON.stringify(output);

}

/**
 * @param {subsidytracker.network.RegisterComplaint} RegisterComplaint
 * @transaction
 */
async function RegisterComplaint(tx) {

    let event = getFactory().newEvent('subsidytracker.network', 'ComplaintRegistered');
    event.citizenId = tx.citizenId;
    event.sackId = tx.sackId;
    event.complaintText = tx.complaintText
    emit(event);
}

/**
 * @param {subsidytracker.network.TraceFoodSack} TraceFoodSack
 * @returns{String}
 * @transaction
 */
async function TraceFoodSack(tx) {
    const sackRegistry = await getAssetRegistry('subsidytracker.network.FoodSack');
    const sack = await sackRegistry.get(tx.sackId);
    let output = {};
    output["quantity"] = sack.quantity
    output["itemName"] = sack.itemName
    output["qualityStatus"] = sack.qualityStatus
    // proc centre
    const procCentreRegistry = await getParticipantRegistry('subsidytracker.network.ProcCentre');
    const procCentre = await procCentreRegistry.get(sack.procCentreId)
    output["procCentre"] = procCentre
    // warehouse
    // if (sack.warehouseId!=" ") {
    //     const warehouseRegistry = await getParticipantRegistry('subsidytracker.network.Warehouse');
    //     const warehouse = await warehouseRegistry.get(sack.warehouseId);
    //     output["warehouse"] = warehouse
    // }
    // else
    //     output["warehouse"] = ""
    // shop
    if (sack.shopId!=" ") {
        const shopRegistry = await getParticipantRegistry('subsidytracker.network.Shop');
        const shop = await shopRegistry.get(sack.shopId);
        output["shop"] = shop
    }
    else
        output["shop"] = ""

    // retrive all quality checks
    let qcList = []
    const allQCList = await query('selectAllQualityChecks')
    for (qc of allQCList) {
        if(qc.eventsEmitted[0].sackId == tx.sackId) {
            qcList.push(qc.eventsEmitted[0])
        }
    }
    output["QCs"] = qcList
    
    // retrieve all invoked subsidies
    let subsidyList = []
    const allSubsidyList = await query('selectAllSubsidies')
    for (subsidy of allSubsidyList) {
        if(subsidy.eventsEmitted[0].sackId == tx.sackId) {
            subsidyList.push(subsidy.eventsEmitted[0])
        }
    }
    output["subsidies"] = subsidyList
    return JSON.stringify(output);
}


