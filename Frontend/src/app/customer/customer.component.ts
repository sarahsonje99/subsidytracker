import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  providers: [SessionStorageService, DatePipe]
})
export class CustomerComponent implements OnInit {

  complaints = []

  constructor(private http: Http, private router: Router, private sstorage: SessionStorageService, private datePipe: DatePipe) { }
  ngOnInit() {
    let shopId = this.sstorage.retrieve("shopID");
    this.http.get("http://localhost:3000/api/RegisterComplaint")
    .subscribe((data:any) => {
      console.log(data._body);
      this.complaints = JSON.parse(data._body);

      for(var i=0;i<this.complaints.length;i++) {
        this.complaints[i].timestamp = this.datePipe.transform(this.complaints[i].timestamp,"yyyy-MM-dd");
      }
    });
  }

  viewCustomer() {
    this.router.navigate(['customer']);
  }

  viewLogs() {
    this.router.navigate(['logs-shop']);
  }
  
  track() {
    this.router.navigate(['track']);
  }

  addViolations() {
    this.router.navigate(['violation-shop']);
  }
  logOut() {
    this.router.navigate(['home-shop']); 
  }

  invoke() {
    this.router.navigate(['invoke']);
  }
}
