import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [SessionStorageService]
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, public sstorage:SessionStorageService) { }

  ngOnInit() {
    this.sstorage.store("userID","a1");
  }

  viewLogs() {
    this.router.navigate(['logs-consumer']);
  }
  
  viewApplications() {
    this.router.navigate(['provision']);
  }

  addViolations() {
    this.router.navigate(['violation']);
  }

  invoke() {
    this.router.navigate(['invoke']);
  }
}
