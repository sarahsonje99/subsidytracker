import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValToIssComponent } from './val-to-iss.component';

describe('ValToIssComponent', () => {
  let component: ValToIssComponent;
  let fixture: ComponentFixture<ValToIssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValToIssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValToIssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
