import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-val-to-iss',
  templateUrl: './val-to-iss.component.html',
  styleUrls: ['./val-to-iss.component.css']
})
export class ValToIssComponent implements OnInit {
	timeline=[];
	chosenPermit=[];

	application:{"$class":string,"permitName":string,"aadhaarId":string,"timestamp":string}={"$class":"digitracker.network.GetPermitHistory","permitName":"","aadhaarId":"","timestamp":""};
	issueData={
		"$class": "digitracker.network.IssuePermit",
		"permitName": "",
		"aadhaarId": "",
		"timestamp": ""
	};
	rejectData:{"$class":string,"rejectionStage":string,"rejectedBecause":string,"permitName":string,"aadhaarId":string,"timestamp":string}={"$class":"digitracker.network.RejectApplication","rejectionStage":"ISSUED","rejectedBecause":"","permitName":"","aadhaarId":"","timestamp":""}; 
	showRejectionDesc:boolean=false;
	Description = new FormControl('');
	
  constructor(private userservice:UserService,private router:Router,private sstorage:SessionStorageService) {
		this.userservice.openIssualForm.subscribe(data=>{
      if(data==true){
				this.animation();
         this.displayIssuance();
      }
    }); 
	 }

  ngOnInit() {
		this.displayIssuance();
		//this.fetchTimeline();
    this.animation();
	}
	
	displayIssuance()
	{
		var x=this.sstorage.retrieve('chosenIssualPermit');
		var y=x.split("#");
		// fetching permit data
    this.userservice.reviewSpecificPermit(y[0],y[1]).subscribe(data=>{
      console.log(data);
      this.chosenPermit.push(data);
		});
		//fetching permit lifecycle
		this.application.permitName=y[0];
		this.application.aadhaarId=y[1];
		this.application.timestamp=new Date().toISOString();
		this.userservice.viewTimeline(JSON.stringify(this.application)).subscribe((data)=>{
		this.timeline=JSON.parse(data).eachStage;
			console.log(data);
		});
	}

// --------------------animation--------------------
  animation(){
    // Vertical Timeline - by CodyHouse.co
	function VerticalTimeline( element ) {
		this.element = element;
		this.blocks = this.element.getElementsByClassName("js-cd-block");
		this.images = this.element.getElementsByClassName("js-cd-img");
		this.contents = this.element.getElementsByClassName("js-cd-content");
		this.offset = 0.8;
		this.hideBlocks();
	};

	VerticalTimeline.prototype.hideBlocks = function() {
		//hide timeline blocks which are outside the viewport
		if ( !("classList" in document.documentElement )) {
			return;
		}
		var self = this;
		for( var i = 0; i < this.blocks.length; i++) {
			(function(i){
				if( self.blocks[i].getBoundingClientRect().top > window.innerHeight*self.offset ) {
					self.images[i].classList.add("cd-is-hidden"); 
					self.contents[i].classList.add("cd-is-hidden"); 
				}
			})(i);
		}
	};

	VerticalTimeline.prototype.showBlocks = function() {
		if ( !("classList" in document.documentElement ) ) {
			return;
		}
		var self = this;
		for( var i = 0; i < this.blocks.length; i++) {
			(function(i){
				if( self.contents[i].classList.contains("cd-is-hidden") && self.blocks[i].getBoundingClientRect().top <= window.innerHeight*self.offset ) {
					// add bounce-in animation
					self.images[i].classList.add("cd-timeline__img--bounce-in");
					self.contents[i].classList.add("cd-timeline__content--bounce-in");
					self.images[i].classList.remove("cd-is-hidden");
					self.contents[i].classList.remove("cd-is-hidden");
				}
			})(i);
		}
	};

	var verticalTimelines = document.getElementsByClassName("js-cd-timeline"),
		verticalTimelinesArray = [],
		scrolling = false;
	if( verticalTimelines.length > 0 ) {
		for( var i = 0; i < verticalTimelines.length; i++) {
			(function(i){
				verticalTimelinesArray.push(new VerticalTimeline(verticalTimelines[i]));
			})(i);
		}

		//show timeline blocks on scrolling
		document.getElementsByClassName("mat-drawer-content")[0].addEventListener("scroll", function(event) {
			console.log("hi");
			if( !scrolling ) {
				scrolling = true;
				(!requestAnimationFrame) ? setTimeout(checkTimelineScroll, 250) : requestAnimationFrame(checkTimelineScroll);
			}
		});
	}

	function checkTimelineScroll() {
		console.log("hi");
		verticalTimelinesArray.forEach(function(timeline){
			timeline.showBlocks();
		});
		scrolling = false;
	};
}
// --------------------animation--------------------


rejectDescription()
  {
    this.showRejectionDesc=true;
  }
  
  finalReject()
  {
    if(this.Description.value!='')
    {
      this.rejectData.rejectedBecause=this.Description.value;
      this.rejectData.permitName=this.chosenPermit[0].permitName;
      this.rejectData.aadhaarId=this.chosenPermit[0].aadhaarId;
      this.rejectData.timestamp=new Date().toISOString();
      this.userservice.rejectApplication(JSON.stringify(this.rejectData)).subscribe(data=>{
          console.log(data);
          this.sstorage.clear('chosenVerifiedPermit');
          this.router.navigate(['forms-rto']);
      });
    }   
	}
	

	issue()
	{
		var x=this.sstorage.retrieve('chosenIssualPermit');
		var y=x.split("#");
		this.issueData.permitName=y[0];
		this.issueData.aadhaarId=y[1];
		this.issueData.timestamp=new Date().toISOString();
		this.userservice.postIssuedData(JSON.stringify(this.issueData)).subscribe(data=>{
			console.log(data);
			this.sstorage.clear('chosenIssualPermit');
			this.router.navigate(['forms-rto']);
		});
	}

}
