import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatSort,MatFormFieldModule, MatInputModule } from '@angular/material';
import { formsForRto } from '../shared/model/formsForRto.model';
import { UserService } from '../shared/service/user.service';
import { ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-forms-rto',
  templateUrl: './forms-rto.component.html',
  styleUrls: ['./forms-rto.component.css']
})
export class FormsRtoComponent implements OnInit,AfterViewInit {

  selectedtype = '';
  selectedstatus='';
  allPermitsMetaData=[];
  public displayedColumns = ['aadhar', 'formName', 'stage','details'];
  public dataSource1 = new MatTableDataSource<formsForRto>();

  @ViewChild(MatSort) sort: MatSort;

  constructor(private userservice:UserService,private router:Router,private sstorage:SessionStorageService) { 
   
  }

  ngOnInit() {
    this.sstorage.clear('chosenAppliedPermit');
    this.sstorage.clear('chosenVerifiedPermit');
    // setting filter predicate for forms
    this.dataSource1.filterPredicate = 
  (data: formsForRto, filtersJson: string) => {
      const matchFilter = [];
      const filters = JSON.parse(filtersJson);

      filters.forEach(filter => {
        const val = data[filter.id] === null ? '' : data[filter.id];
        matchFilter.push(val.toLowerCase().includes(filter.value.toLowerCase()));
      });
        return matchFilter.every(Boolean);
    };
    //getting filter for permit names created by rto
    this.userservice.allPermitsMetaData().subscribe(data=>{
      console.log(data);
        this.allPermitsMetaData.push(data);
    });
    //getting all the forms 
    this.getFormStatus();
  }

  ngAfterViewInit(): void {
    this.dataSource1.sort = this.sort;
  }

  getFormStatus()
  {
    var x=[];
    this.userservice.reviewPermits(this.sstorage.retrieve('iacentre')).subscribe(
      res => {
        x.push(res);
        setTimeout(() => {
          this.dataSource1.data=x;
        }, 2000);
        console.log(res);
       // this.dataSource.data.push(res as formsForRto); 
      }  
    );
  }

public doFilter = (value: string) => {
    this.dataSource1.filter = value.trim().toLocaleLowerCase();
  }

  FilterType(type)
  {
    const tableFilters = [];
    tableFilters.push({
      id: 'permitName',
      value: this.selectedtype
    });
    tableFilters.push({
      id: 'stage',
      value: this.selectedstatus
    });

    this.dataSource1.filter = JSON.stringify(tableFilters);
    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
    // console.log(type.value);
    // if(type.value!="all")
    // {
    //   this.dataSource.filter = type.value.trim().toLocaleLowerCase();
    //   console.log(this.dataSource.data);
    // }
    // else{
    //   var x:string="business";
    //   this.dataSource.filter=x.trim().toLocaleLowerCase();
    // }
    
   
  }

  redirectToDetails(chosenPermit)
  {
    console.log(chosenPermit.stage);
    if(chosenPermit.stage=='APPLIED')
    {
       this.sstorage.clear('chosenAppliedPermit');
      this.sstorage.store('chosenAppliedPermit',chosenPermit.permitId);      
       this.userservice.openAppliedForm.emit(true);
       this.router.navigate(['apl-to-ver']);
    }
    else if(chosenPermit.stage=='VERIFIED')
    {
      if(chosenPermit.psychEval==true && chosenPermit.doctorCertificate=='-1')
      {
        this.sstorage.clear('psychPermit');
        this.sstorage.store('psychPermit',chosenPermit.permitId);
        this.router.navigate(['psych']);
      }
      else
      {
        this.userservice.getIsValidationStage(chosenPermit.permitName).subscribe(data=>{
      
         
          if(data['validationRequired']==true)
          {
            this.sstorage.clear('chosenVerifiedPermit');
            this.sstorage.store('chosenVerifiedPermit',chosenPermit.permitId); 
            this.userservice.openVerifiedForm.emit(true);
            this.router.navigate(['ver-to-val']);
          }
          else if(data['validationRequired']==false)
          {
            this.sstorage.clear('chosenIssualPermit');
            this.sstorage.store('chosenIssualPermit',chosenPermit.permitId); 
            this.userservice.openIssualForm.emit(true);
            this.router.navigate(['val-to-iss']);
          }
                
    });
  }
}
    else if(chosenPermit.stage=='VALIDATED')
    {
      this.sstorage.clear('chosenIssualPermit');
      this.sstorage.store('chosenIssualPermit',chosenPermit.permitId); 
      this.userservice.openIssualForm.emit(true);
      this.router.navigate(['val-to-iss']);
    }
  }

}
