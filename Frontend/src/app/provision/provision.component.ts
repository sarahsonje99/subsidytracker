import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-provision',
  templateUrl: './provision.component.html',
  styleUrls: ['./provision.component.css'],
  providers: [SessionStorageService]
})
export class ProvisionComponent implements OnInit {

  provisions:any
  citizenId: any
  prov:any
  invoked:any

  constructor(private http: Http, private router: Router, private sstorage: SessionStorageService) { }

  ngOnInit() {
    this.http.get("http://localhost:3000/api/Citizen/"+this.sstorage.retrieve("userID"))
    .subscribe((data:any) => {
      console.log(data._body);
      this.provisions = JSON.parse(data._body);
      this.citizenId = this.sstorage.retrieve("userID")
      console.log(this.provisions);
      this.prov = JSON.parse(this.provisions.quantity);
      console.log(this.prov)
      this.invoked = JSON.parse(this.provisions.invokedInMonth);
      console.log(this.invoked)
      this.invoked.Wheat = parseInt(this.invoked.Wheat)
    });
  }

  viewLogs() {
    this.router.navigate(['logs-consumer']);
  }
  
  viewApplications() {
    this.router.navigate(['provision']);
  }

  addViolations() {
    this.router.navigate(['violation']);
  }

  logOut() {
    this.router.navigate(['']); 
  }

  invoke() {
    this.router.navigate(['invoke']);
  }
  
}
