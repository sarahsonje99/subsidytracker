import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-logs-consumer',
  templateUrl: './logs-consumer.component.html',
  styleUrls: ['./logs-consumer.component.css'],
  providers: [SessionStorageService, DatePipe]
})
export class LogsConsumerComponent implements OnInit {

  logs = [];

  constructor(private http: Http, private router: Router, private sstorage: SessionStorageService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.http.get("http://localhost:3000/api/InvokeSubsidy?filter=%7B%22where%22%3A%7B%22citizenId%22%3A%20%22"+this.sstorage.retrieve("userID")+"%22%7D%7D")
    .subscribe((data:any) => {
      console.log(data._body);
      this.logs = JSON.parse(data._body);

      for(var i=0;i<this.logs.length;i++) {
        this.logs[i].timestamp = this.datePipe.transform(this.logs[i].timestamp,"yyyy-MM-dd");
      }
      
      console.log(this.logs);
    });
  }

  viewLogs() {
    this.router.navigate(['logs-consumer']);
  }
  
  viewApplications() {
    this.router.navigate(['provision']);
  }

  addViolations() {
    this.router.navigate(['violation']);
  }

  logOut() {
    this.router.navigate(['']); 
  }

  invoke() {
    this.router.navigate(['invoke']);
  }
}
