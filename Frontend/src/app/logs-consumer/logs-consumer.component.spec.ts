import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsConsumerComponent } from './logs-consumer.component';

describe('LogsConsumerComponent', () => {
  let component: LogsConsumerComponent;
  let fixture: ComponentFixture<LogsConsumerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsConsumerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsConsumerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
