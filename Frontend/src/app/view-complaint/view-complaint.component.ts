import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-view-complaint',
  templateUrl: './view-complaint.component.html',
  styleUrls: ['./view-complaint.component.css'],
  providers: [DatePipe]
})
export class ViewComplaintComponent implements OnInit {

  complaints = []

  constructor(private router: Router, private http: Http, private datePipe: DatePipe) { }

  ngOnInit() {
    this.http.get('http://localhost:3000/api/RegisterComplaint')
    .subscribe((data:any)=> {
      this.complaints = JSON.parse(data._body);
      console.log(this.complaints);

      for(var i=0;i<this.complaints.length;i++) {
        this.complaints[i].timestamp = this.datePipe.transform(this.complaints[i].timestamp,"yyyy-MM-dd");
      }
    })
  }

  viewFPS() {
    this.router.navigate(['fps']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  addSubsidy() {
    this.router.navigate(['add-subsidy']);
  }

  send() {
    this.router.navigate(['send']);
  }

  viewComplaints() {
    this.router.navigate(['view-violation']);
  }

  logOut() {
    this.router.navigate(['home-gov']);
  }

}
