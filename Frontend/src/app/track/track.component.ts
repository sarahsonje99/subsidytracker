import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {

  tracker: boolean;
  goods = []

  sackId = new FormControl('');

  constructor(private http: Http, private router: Router) { }
  ngOnInit() {
    // this.http.get("http://localhost:4100/api/goods")
    // .subscribe((data:any) => {
    //   console.log(data._body);
    //   this.provision = data._body.logs;
    // });
    this.tracker = false;
  }

  viewCustomer() {
    this.router.navigate(['customer']);
  }

  viewLogs() {
    this.router.navigate(['logs-shop']);
  }
  
  track() {
    this.router.navigate(['track']);
  }

  addViolations() {
    this.router.navigate(['violation-shop']);
  }

  logOut() {
    this.router.navigate(['home-shop']); 
  }
  invoke() {
    this.router.navigate(['invoke']);
  }

  submit() {
    this.http.post('http://localhost:3000/api/TraceFoodSack', {
      "$class": "subsidytracker.network.TraceFoodSack",
      "sackId": this.sackId.value,
      "timestamp": new Date()
    }).subscribe((data:any) => {
      this.goods = JSON.parse(JSON.parse(data._body));
      console.log(this.goods);
      this.tracker = true;
    });
  }


}
