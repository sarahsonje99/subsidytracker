import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { UserService } from '../shared/service/user.service';

@Component({
  selector: 'app-login-gov',
  templateUrl: './login-gov.component.html',
  styleUrls: ['./login-gov.component.css']
})
export class LoginGovComponent implements OnInit {

  national= new FormControl('');
  authority = new FormControl('');
  password = new FormControl('');

  constructor(private http: Http, private router: Router,private sstorage: SessionStorageService,private userservice:UserService) { }

  ngOnInit() {
  }

  invalidDetails() {
    let length = this.password.value.length;
    let l2 = this.national.value.length;
    var isnum2 = /^\d+$/.test(this.national.value);
    if(length < 4 || l2 != 12)
      return false;
    else if(!isnum2)
      return false;
    else if(this.password.value.search(/\d/) == -1)
      return false;
    else if (this.password.value.search(/[a-z]/) == -1) 
      return false;
    else if (this.password.value.search(/[A-Z]/) == -1)
      return false;
    else if (this.password.value.search(/[^a-zA-Z0-9\!\@\#\$\%\^\&\*\(\)\_\+]/) != -1)
        return false;
    else
      return true;
  }

  loginGov(){
    // Rest API call for login of Issuing Authority
    this.http.post('http://localhost:4100/api/login-gov', {
      national: this.national.value,
      authority: this.authority.value,
      password: this.password.value
    }).subscribe((data:any) => {
      console.log(data)
      let data1 = JSON.parse(data._body)
      if(data1.auth == true) {
        this.sstorage.store('gov-auth',this.authority.value);
        this.sstorage.store("loggedInGov",true);
        this.userservice.loggedIA.emit(true);
        console.log("LOGIN SUCCESSFUL");
        this.router.navigate(['formgen']);
      }
    });
  }
}
