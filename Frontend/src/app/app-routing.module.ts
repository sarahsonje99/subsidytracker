import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { FormgenComponent } from './formgen/formgen.component';
import { UserpermitComponent } from './userpermit/userpermit.component';
import { PaymentRequestComponent } from './payment-request/payment-request.component';
import { LoginUserComponent } from './login-user/login-user.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { FormsRtoComponent } from './forms-rto/forms-rto.component';
import { PaySuccessComponent } from './pay-success/pay-success.component';
import { FormrenComponent } from './formren/formren.component';
import { AplToVerComponent } from './apl-to-ver/apl-to-ver.component';
import { VerToValComponent } from './ver-to-val/ver-to-val.component';
import { ValToIssComponent } from './val-to-iss/val-to-iss.component';
import { HomeComponent } from './home/home.component';
import { ViolationComponent } from './violation/violation.component';
import { PsychComponent } from './psych/psych.component';
import { LoginGovComponent } from './login-gov/login-gov.component';
import { LoginQcComponent } from './login-qc/login-qc.component';
import { LoginShopComponent } from './login-shop/login-shop.component';
import { LogsConsumerComponent } from './logs-consumer/logs-consumer.component';
import { ProvisionComponent } from './provision/provision.component';
import { HomeShopComponent } from './home-shop/home-shop.component';
import { CustomerComponent } from './customer/customer.component';
import { TrackComponent } from './track/track.component';
import { LogsShopComponent } from './logs-shop/logs-shop.component';
import { ViolationShopsComponent } from './violation-shops/violation-shops.component';
import { HomeGovComponent } from './home-gov/home-gov.component';
import { FpsComponent } from './fps/fps.component';
import { ViewComplaintComponent } from './view-complaint/view-complaint.component';
import { LogsGovComponent } from './logs-gov/logs-gov.component';
import { SendGoodsComponent } from './send-goods/send-goods.component';
import { HomeQcComponent } from './home-qc/home-qc.component';
import { QualityCheckComponent } from './quality-check/quality-check.component';
import { ShopsQcComponent } from './shops-qc/shops-qc.component';
import { AddFoodComponent } from './add-food/add-food.component';
import { InvokeComponent } from './invoke/invoke.component';

const approutes:Routes=[
    { path: '', component: HomeComponent},
    { path: 'home-shop', component: HomeShopComponent},
    { path: 'invoke', component: InvokeComponent},
    { path: 'home-qc', component: HomeQcComponent},
    { path: 'add-subsidy', component: AddFoodComponent},
    { path: 'quality-check', component: QualityCheckComponent},
    { path: 'shop-qc', component: ShopsQcComponent},
    { path: 'send', component: SendGoodsComponent},
    { path: 'fps', component: FpsComponent},
    { path: 'home-gov', component: HomeGovComponent},
    { path: 'login-gov', component: LoginGovComponent },
    { path: 'logs-gov', component: LogsGovComponent },
    { path: 'view-violation', component: ViewComplaintComponent },
    { path: 'track', component: TrackComponent },
    { path: 'provision', component: ProvisionComponent },
    { path: 'login-qc', component: LoginQcComponent },
    { path: 'login-shop', component: LoginShopComponent },
    { path: 'customer', component: CustomerComponent},
    { path: 'login', component: LoginUserComponent },
    { path: 'logs-shop', component: LogsShopComponent},
    { path: 'register', component: RegisterComponent },
    { path: 'logs-consumer', component: LogsConsumerComponent },
    { path: 'formgen', component: FormgenComponent },
    { path: 'mypermit', component: UserpermitComponent },
    { path: 'payment', component: PaymentRequestComponent },
    { path: 'file-upload', component: FileuploadComponent },
    { path: 'forms-rto', component: FormsRtoComponent },
    { path: 'formren', component: FormrenComponent },
    { path: 'apl-to-ver', component: AplToVerComponent},
    { path: 'ver-to-val', component: VerToValComponent},
    { path: 'val-to-iss', component: ValToIssComponent},
    { path: 'pay-success', component: PaySuccessComponent },
    { path: 'violation', component: ViolationComponent },
    { path: 'violation-shop', component: ViolationShopsComponent },
    { path: 'psych', component:PsychComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(approutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
