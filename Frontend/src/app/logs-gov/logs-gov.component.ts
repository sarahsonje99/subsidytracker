import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-logs-gov',
  templateUrl: './logs-gov.component.html',
  styleUrls: ['./logs-gov.component.css'],
  providers: [SessionStorageService, DatePipe]
})
export class LogsGovComponent implements OnInit {

  logs = [];

  constructor(private http: Http, private router: Router, private sstorage: SessionStorageService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.http.get("http://localhost:3000/api/InvokeSubsidy")
    .subscribe((data:any) => {
      console.log(data._body);
      this.logs = JSON.parse(data._body);
      for(var i=0;i<this.logs.length;i++) {
        this.logs[i].timestamp = this.datePipe.transform(this.logs[i].timestamp,"yyyy-MM-dd");
      }
    });
  }

  viewFPS() {
    this.router.navigate(['fps']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  send() {
    this.router.navigate(['send']);
  }

  addSubsidy() {
    this.router.navigate(['add-subsidy']);
  }

  viewComplaints() {
    this.router.navigate(['view-violation']);
  }

  logOut() {
    this.router.navigate(['home-gov']);
  }

}
