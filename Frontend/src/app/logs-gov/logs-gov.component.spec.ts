import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsGovComponent } from './logs-gov.component';

describe('LogsGovComponent', () => {
  let component: LogsGovComponent;
  let fixture: ComponentFixture<LogsGovComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsGovComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsGovComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
