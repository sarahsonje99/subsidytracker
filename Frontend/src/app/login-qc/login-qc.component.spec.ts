import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginQcComponent } from './login-qc.component';

describe('LoginQcComponent', () => {
  let component: LoginQcComponent;
  let fixture: ComponentFixture<LoginQcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginQcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginQcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
