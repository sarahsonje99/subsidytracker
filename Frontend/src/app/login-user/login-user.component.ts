import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SessionStorageService } from 'ngx-webstorage';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { MatDialog,MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css'],
  providers:[SessionStorageService]
})
export class LoginUserComponent implements OnInit {

  //@Output()logging=new EventEmitter<{loggedIn:boolean}>();
  message:string;
  mobile = new FormControl('');
  national = new FormControl('');
  otp = new FormControl('');
  requestId: any;

  messageFromServer: String;
  ws: WebSocket;

  loggedIn:boolean=false;
  bool: boolean;
  mobileNumber: any;

  userDetailJson=[];
  userDetails:{mobileNumber:number,nationalId:string}={mobileNumber:0,nationalId:""};
  
  constructor(public dialogRef: MatDialogRef<LoginUserComponent>,private userservice:UserService,public sstorage:SessionStorageService,private router:Router, private http: Http) { }

  ngOnInit() {
    this.bool = true;
    this.sstorage.store("loggedIn",false);

  }

  invalidDetails() {
    let lengthNational = this.national.value.length;
    var isnum2 = /^\d+$/.test(this.national.value);
    if(lengthNational != 12)
      return false;
    else if(!isnum2)
      return false
    else
      return true;
  }

  getOtp() {
    this.http.post("http://localhost:4100/api/login",({
      "national": this.national.value
    }))
    .subscribe((data: any) =>{
      console.log(data)
      let data1 = JSON.parse(data._body);
      this.requestId = data1.requestId;
      this.mobileNumber = data1.mobile;
      if(this.mobileNumber != undefined){
        let m = this.mobileNumber.split('');
        for(let i=0;i<m.length-2;i++) {
          m[i] = '*';
        }
        this.mobileNumber = '+91'+m.join('');
        console.log(this.requestId);
        this.bool = false;
      }
    },error=>{
      console.log(error);
    });
  }

  invalidOtp() {
    let length = this.otp.value.length;
    var isnum = /^\d+$/.test(this.otp.value);
    if(length != 4)
      return false;
    else if(!isnum)
      return false
    else
      return true;
  }

  login() {
    this.userDetails.mobileNumber=this.mobile.value;
    this.userDetails.nationalId=this.national.value;
    this.userDetailJson.push(this.userDetails);
    this.http.post("http://localhost:4100/api/verify-otp",{
      pin: this.otp.value,
      requestId: this.requestId
    })
    .subscribe((data: any) =>{
      let message = JSON.parse(data._body).message;
      console.log(message)
      if(message === 'Account verified!') {
        this.loggedIn=true;
        this.sstorage.store("userId",this.national.value);
        this.sstorage.store("loggedIn",true);
        this.userservice.logging.emit(true);
        this.dialogRef.close();
        this.router.navigate(['']);
        console.log("LOGGED IN"+this.loggedIn)
      }
      else {
        alert('INCORRECT PIN! Please try again');
      }
      console.log(this.requestId)
    },error=>{
        console.log(error);
    });
  }
}
