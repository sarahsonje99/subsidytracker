import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { Http } from '@angular/http';

@Component({
  selector: 'app-violation',
  templateUrl: './violation.component.html',
  styleUrls: ['./violation.component.css'],
  providers:[DatePipe, SessionStorageService]
})
export class ViolationComponent implements OnInit {

  description = new FormControl('');
  orderNumber = new FormControl('');
  sackId = new FormControl('');
  alert: boolean;

  constructor(private router:Router,private datePipe: DatePipe, private userService: UserService, public sstorage:SessionStorageService, private http: Http) { }

  ngOnInit() {
    this.alert = false;
  }

  submit(){
    this.http.post('http://localhost:3000/api/RegisterComplaint', {
      "$class": "subsidytracker.network.RegisterComplaint",
      "citizenId": this.sstorage.retrieve("userID"),
      "sackId": this.orderNumber.value,
      "complaintText": this.description.value,
      "transactionId": "",
      "timestamp": new Date()
    }).subscribe((data:any)=> {
      console.log(data._body);
      this.alert = true;
    })
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy'); //whatever format you need.
  }

  viewLogs() {
    this.router.navigate(['logs-consumer']);
  }
  
  viewApplications() {
    this.router.navigate(['provision']);
  }

  addViolations() {
    this.router.navigate(['violation']);
  }

  logOut() {
    this.router.navigate(['']); 
  }

  invoke() {
    this.router.navigate(['invoke']);
  }
}
