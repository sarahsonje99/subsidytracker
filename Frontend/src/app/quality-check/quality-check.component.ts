import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { SessionStorageService } from 'ngx-webstorage';
import { Http } from '@angular/http';

@Component({
  selector: 'app-quality-check',
  templateUrl: './quality-check.component.html',
  styleUrls: ['./quality-check.component.css'],
  providers: [SessionStorageService]
})
export class QualityCheckComponent implements OnInit {

  location = new FormControl('');
  sackId = new FormControl('');
  shopId = new FormControl('');
  status = new FormControl('');
  alert: boolean

  constructor(private router:Router, private sstorage: SessionStorageService, private http: Http) { }

  ngOnInit() {
    this.alert = false;
  }


  viewShop() {
    this.router.navigate(['shop-qc']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  check() {
    this.router.navigate(['quality-check']);
  }

  submit() {
    this.http.post("http://localhost:3000/api/CheckQuality", {
      "$class": "subsidytracker.network.CheckQuality",
      "location": this.location.value,
      "locationId": this.shopId.value,
      "QCId": this.sstorage.retrieve("qcId"),
      "sackId": this.sackId.value,
      "status": this.status.value,
      "transactionId": "",
      "timestamp": new Date()
    })
    .subscribe((data:any) => {
      console.log(JSON.parse(data._body));
      //this.router.navigate(['home-qc']);
      this.alert = true;
    })
  }

  logOut() {
    this.router.navigate(['home-qc']);
  }

}
