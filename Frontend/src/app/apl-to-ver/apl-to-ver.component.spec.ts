import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplToVerComponent } from './apl-to-ver.component';

describe('AplToVerComponent', () => {
  let component: AplToVerComponent;
  let fixture: ComponentFixture<AplToVerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplToVerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplToVerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
