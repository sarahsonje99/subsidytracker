import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/service/user.service';
import { SessionStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-apl-to-ver',
  templateUrl: './apl-to-ver.component.html',
  styleUrls: ['./apl-to-ver.component.css']
})
export class AplToVerComponent implements OnInit {

  psychscore={"$class":"digitracker.network.UpdateNlpScore","nlpScore":0.75,"aadhaarId":"","permitName":"","timestamp":""};
  chosenPermit=[];
  chosenPermitJSON=[];
  requiredDocsJSON={"sign":"iVBORw0KGgoAAAANSUhEUgAAAA4AAAANCAIAAAAWvsgoAAAAA3NCSVQICAjb4U/gAAAAGXRFWHRTb2Z0d2FyZQBnbm9tZS1zY3JlZW5zaG907wO/PgAAAExJREFUKJFj/PXrFwNxgIlIdaQpZTl79uyaNWuwyhkbG4eEhCBMRePjUgd1AKYoVv1MmHK47GFBNomBgeH+/ftY1TEwMDAOdLjSRikA56YZqSMrYbQAAAAASUVORK5CYII=","addressProof":""};
  keys=["sign","addressProof"];
  filestring:string='';
  postData:{"$class":string,"permitName":string,"aadhaarId":string,"timestamp":string}={"$class":"digitracker.network.VerifyApplication","permitName":"","aadhaarId":"","timestamp":""};


  rejectData:{"$class":string,"rejectionStage":string,"rejectedBecause":string,"permitName":string,"aadhaarId":string,"timestamp":string}={"$class":"digitracker.network.RejectApplication","rejectionStage":"VERIFIED","rejectedBecause":"","permitName":"","aadhaarId":"","timestamp":""};
  showRejectionDesc:boolean=false;
  Description = new FormControl('');
  constructor(private router:Router,private userservice:UserService,private sstorage:SessionStorageService) { 
    this.userservice.openAppliedForm.subscribe(data=>{
      if(data==true){
         this.displayForm();
      }
    });
  }

  ngOnInit() {
   this.displayForm();
  }

  displayForm()
  {
    var x=this.sstorage.retrieve('chosenAppliedPermit');
    var y=x.split("#");
    this.userservice.reviewSpecificPermit(y[0],y[1]).subscribe(data=>{
      console.log(data);
      this.chosenPermit.push(data);
      this.chosenPermitJSON=JSON.parse(data.formJSON);
      this.requiredDocsJSON=JSON.parse(data.requiredDocsJSON);
      this.keys=Object.keys(JSON.parse(data.requiredDocsJSON));
    });
  }

  viewImage(imgtype)
  {
    this.filestring=this.requiredDocsJSON[imgtype];
  }

  verify()
  {
    
    this.postData.permitName=this.chosenPermit[0].permitName;
    this.postData.aadhaarId=this.chosenPermit[0].aadhaarId;
    this.postData.timestamp=new Date().toISOString();

    this.psychscore.permitName=this.chosenPermit[0].permitName;
    this.psychscore.aadhaarId=this.chosenPermit[0].aadhaarId;
    this.psychscore.timestamp=new Date().toISOString();
    console.log(this.postData);
    this.userservice.postVerifiedPermit(JSON.stringify(this.postData)).subscribe(data=>{
      this.userservice.sendNlp(JSON.stringify(this.psychscore)).subscribe(data=>{
        console.log(data);
        this.sstorage.clear('chosenAppliedPermit');
        this.router.navigate(['forms-rto']);
      });     
    });
  }

  rejectDescription()
  {
    this.showRejectionDesc=true;
  }
  
  finalReject()
  {
    if(this.Description.value!='')
    {
      this.rejectData.rejectedBecause=this.Description.value;
      this.rejectData.permitName=this.chosenPermit[0].permitName;
      this.rejectData.aadhaarId=this.chosenPermit[0].aadhaarId;
      this.rejectData.timestamp=new Date().toISOString();
      this.userservice.rejectApplication(JSON.stringify(this.rejectData)).subscribe(data=>{
          console.log(data);
          this.sstorage.clear('chosenAppliedPermit');
          this.router.navigate(['forms-rto']);
      });
    }
    
  }
}
