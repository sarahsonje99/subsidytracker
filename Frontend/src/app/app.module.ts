import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FileSelectDirective } from 'ng2-file-upload';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import { FormgenComponent } from './formgen/formgen.component';
import { UserpermitComponent } from './userpermit/userpermit.component';
import { PaymentRequestComponent } from './payment-request/payment-request.component'
import { LoginUserComponent } from './login-user/login-user.component';
import { FormsRtoComponent } from './forms-rto/forms-rto.component';
import { FileuploadComponent } from './fileupload/fileupload.component';

import { UserService } from './shared/service/user.service';
import { FormService } from './shared/service/form.service';
import { WebsocketService } from './websocket.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { MyMaterialModule } from  './material.module';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatStepperModule } from '@angular/material/stepper';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { PaySuccessComponent } from './pay-success/pay-success.component';
import { FormrenComponent } from './formren/formren.component';
import { AplToVerComponent } from './apl-to-ver/apl-to-ver.component';
import { VerToValComponent } from './ver-to-val/ver-to-val.component';
import { ValToIssComponent } from './val-to-iss/val-to-iss.component';
import { HomeComponent } from './home/home.component';
import { ViolationComponent } from './violation/violation.component';
import { DatePipe } from '@angular/common';
import { PsychComponent } from './psych/psych.component';
import { LoginShopComponent } from './login-shop/login-shop.component';
import { LoginGovComponent } from './login-gov/login-gov.component';
import { LoginQcComponent } from './login-qc/login-qc.component';
import { LogsConsumerComponent } from './logs-consumer/logs-consumer.component';
import { ProvisionComponent } from './provision/provision.component';
import { HomeShopComponent } from './home-shop/home-shop.component';
import { CustomerComponent } from './customer/customer.component';
import { TrackComponent } from './track/track.component';
import { LogsShopComponent } from './logs-shop/logs-shop.component';
import { ViolationShopsComponent } from './violation-shops/violation-shops.component';
import { HomeGovComponent } from './home-gov/home-gov.component';
import { FpsComponent } from './fps/fps.component';
import { SendGoodsComponent } from './send-goods/send-goods.component';
import { HttpClientModule } from '@angular/common/http';
import { ViewComplaintComponent } from './view-complaint/view-complaint.component';
import { LogsGovComponent } from './logs-gov/logs-gov.component';
import { QualityCheckComponent } from './quality-check/quality-check.component';
import { HomeQcComponent } from './home-qc/home-qc.component';
import { ShopsQcComponent } from './shops-qc/shops-qc.component';
import { AddFoodComponent } from './add-food/add-food.component';
import { InvokeComponent } from './invoke/invoke.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    FormgenComponent,
    UserpermitComponent,
    PaymentRequestComponent,
    LoginUserComponent,
    FileSelectDirective,
    FileuploadComponent,
    FormsRtoComponent,
    NavbarComponent,
    FormrenComponent,
    AplToVerComponent,
    VerToValComponent,
    ValToIssComponent,
    PaySuccessComponent,
    HomeComponent,
    ViolationComponent,
    PsychComponent,
    LoginShopComponent,
    LoginGovComponent,
    LoginQcComponent,
    LogsConsumerComponent,
    ProvisionComponent,
    HomeShopComponent,
    CustomerComponent,
    TrackComponent,
    LogsShopComponent,
    ViolationShopsComponent,
    HomeGovComponent,
    FpsComponent,
    SendGoodsComponent,
    ViewComplaintComponent,
    LogsGovComponent,
    QualityCheckComponent,
    HomeQcComponent,
    ShopsQcComponent,
    AddFoodComponent,
    InvokeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    FlexLayoutModule,
    HttpModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule.forRoot()
  ],
  providers: [UserService, FormService, WebsocketService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
