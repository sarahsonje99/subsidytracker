import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-send-goods',
  templateUrl: './send-goods.component.html',
  styleUrls: ['./send-goods.component.css']
})
export class SendGoodsComponent implements OnInit {

  shopId = new FormControl('');
  sackId = new FormControl('');
  
  alert: boolean;
  error: boolean;

  constructor(private router: Router, private http: Http) { }

  ngOnInit() {
    this.alert = false;
    this.error = false;
  }

  viewFPS() {
    this.router.navigate(['fps']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  submit() {
    this.http.post("http://localhost:3000/api/AddToShop", {
      "$class": "subsidytracker.network.AddToShop",
      "shopId": this.shopId.value,
      "sackId": this.sackId.value,
      "timestamp": new Date()
    })
    .subscribe((data:any)=> {
      let result = JSON.parse(data._body);
      console.log(result)
      if(result === "{\"status\":\"Successful\"}"){
        // alert("Goods Transferred Successfully!");
        // this.router.navigate(['home-gov']);
        this.alert = true;
      }
      else {
        //alert("Transfer failed")
        this.error = true;
      }
    });
  }

  viewComplaints() {
    this.router.navigate(['violation-shop']);
  }

  logOut() {
    this.router.navigate(['home-gov']);
  }

  addSubsidy() {
    this.router.navigate(['add-subsidy']);
  }
}
