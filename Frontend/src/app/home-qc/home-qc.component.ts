import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-home-qc',
  templateUrl: './home-qc.component.html',
  styleUrls: ['./home-qc.component.css'],
  providers: [SessionStorageService]
})
export class HomeQcComponent implements OnInit {

  constructor(private router: Router, private sstorage: SessionStorageService) { }

  ngOnInit() {
    this.sstorage.store("qcId","qc1");
  }

  viewShop() {
    this.router.navigate(['shop-qc']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  check() {
    this.router.navigate(['quality-check']);
  }

}
