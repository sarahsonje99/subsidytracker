import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeQcComponent } from './home-qc.component';

describe('HomeQcComponent', () => {
  let component: HomeQcComponent;
  let fixture: ComponentFixture<HomeQcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeQcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeQcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
