import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-home-gov',
  templateUrl: './home-gov.component.html',
  styleUrls: ['./home-gov.component.css'],
  providers: [SessionStorageService]
})
export class HomeGovComponent implements OnInit {

  constructor(private router: Router, private sstorage: SessionStorageService) { }

  ngOnInit() {
    this.sstorage.store("procID", "pc1")
  }

  viewFPS() {
    this.router.navigate(['fps']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  send() {
    this.router.navigate(['send']);
  }

  addSubsidy() {
    this.router.navigate(['add-subsidy']);
  }

  viewComplaints() {
    this.router.navigate(['view-violation']);
  }

}
