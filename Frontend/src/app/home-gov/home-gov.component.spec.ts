import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeGovComponent } from './home-gov.component';

describe('HomeGovComponent', () => {
  let component: HomeGovComponent;
  let fixture: ComponentFixture<HomeGovComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeGovComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeGovComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
