import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsShopComponent } from './logs-shop.component';

describe('LogsShopComponent', () => {
  let component: LogsShopComponent;
  let fixture: ComponentFixture<LogsShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
