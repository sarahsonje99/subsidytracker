import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-logs-shop',
  templateUrl: './logs-shop.component.html',
  styleUrls: ['./logs-shop.component.css'],
  providers: [SessionStorageService, DatePipe]
})
export class LogsShopComponent implements OnInit {

  logs = [];

  constructor(private http: Http, private router: Router, private sstorage: SessionStorageService, private datePipe: DatePipe) { }

  ngOnInit() {
    let shopId = this.sstorage.retrieve("shopID");
    this.http.get("http://localhost:3000/api/InvokeSubsidy?filter=%7B\"where\"%3A%20%7B\"shopId\"%3A%20\""+shopId+"\"%7D%7D")
    .subscribe((data:any) => {
      console.log(data._body);
      this.logs = JSON.parse(data._body);
      for(var i=0;i<this.logs.length;i++) {
        this.logs[i].timestamp = this.datePipe.transform(this.logs[i].timestamp,"yyyy-MM-dd");
      }
    });
  }

  viewCustomer() {
    this.router.navigate(['customer']);
  }

  viewLogs() {
    this.router.navigate(['logs-shop']);
  }
  
  track() {
    this.router.navigate(['track']);
  }

  addViolations() {
    this.router.navigate(['violation-shop']);
  }
  logOut() {
    this.router.navigate(['home-shop']); 
  }

  invoke() {
    this.router.navigate(['invoke']);
  }
}
