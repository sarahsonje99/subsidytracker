import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UserService } from '../shared/service/user.service';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';
import { Http } from '@angular/http';

@Component({
  selector: 'app-violation-shops',
  templateUrl: './violation-shops.component.html',
  styleUrls: ['./violation-shops.component.css'],
  providers: [SessionStorageService]
})
export class ViolationShopsComponent implements OnInit {

  description = new FormControl('');
  orderNumber = new FormControl('');
  date = new FormControl('');
  constructor(private router:Router,private datePipe: DatePipe, private userService: UserService, public sstorage:SessionStorageService, private http: Http) { }

  ngOnInit() {
  }

  submit(){
    this.http.post('http://localhost:3000/api/RegisterComplaint', {
      "$class": "subsidytracker.network.RegisterComplaint",
      "citizenId": this.sstorage.retrieve("shopID"),
      "subsidyId": this.orderNumber.value,
      "complaintText": this.description.value,
      "transactionId": "",
      "timestamp": new Date(this.date.value)
    }).subscribe((data:any)=> {
      console.log(data);
      this.router.navigate(['customer']);
    })
  }

  transformDate(date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy'); //whatever format you need.
  }

  viewCustomer() {
    this.router.navigate(['customer']);
  }

  viewLogs() {
    this.router.navigate(['logs-shop']);
  }
  
  track() {
    this.router.navigate(['track']);
  }

  addViolations() {
    this.router.navigate(['violation-shop']);
  }

  logOut() {
    this.router.navigate(['home-shop']); 
  }

  invoke(){
    this.router.navigate(['invoke']);
  }

}
