import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViolationShopsComponent } from './violation-shops.component';

describe('ViolationShopsComponent', () => {
  let component: ViolationShopsComponent;
  let fixture: ComponentFixture<ViolationShopsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViolationShopsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViolationShopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
