import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { SessionStorageService } from 'ngx-webstorage';
import { DatePipe } from '@angular/common';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-food',
  templateUrl: './add-food.component.html',
  styleUrls: ['./add-food.component.css'],
  providers: [SessionStorageService, DatePipe]
})
export class AddFoodComponent implements OnInit {

  quantity = new FormControl('');
  itemName = new FormControl('');
  alert: boolean;

  img='assets/img/sucess.png';


  constructor(private router: Router, private http: Http, private sstorage: SessionStorageService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.alert = false;
  }

  viewFPS() {
    this.router.navigate(['fps']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  send() {
    this.router.navigate(['send']);
  }

  viewComplaints() {
    this.router.navigate(['view-violation']);
  }

  addSubsidy() {
    this.router.navigate(['add-subsidy']);
  }

  logOut() {
    this.router.navigate(['home-gov']);
  }

  submit() {
    var num = ""
    for(var i=0;i<this.sstorage.retrieve("procID").length;i++) {
      num += this.sstorage.retrieve("procID")[i].charCodeAt(0);
    }
    let sackID = Number(new Date())+""+num;
    this.http.post('http://localhost:3000/api/FoodSack',{
      "$class": "subsidytracker.network.FoodSack",
      "foodSackId": sackID,
      "quantity": this.quantity.value,
      "itemName": this.itemName.value,
      "procCentreId": this.sstorage.retrieve("procID"),
      "shopId": " ",
      "qualityStatus": " "
    })
    .subscribe((data:any)=> {
      console.log(JSON.parse(data._body));
      //alert('Successfully added new Procured Sack!')
      this.alert = true;
      //this.router.navigate(['home-gov'])
    });
  }

}
