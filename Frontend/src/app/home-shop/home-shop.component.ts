import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SessionStorageService } from 'ngx-webstorage';


@Component({
  selector: 'app-home-shop',
  templateUrl: './home-shop.component.html',
  styleUrls: ['./home-shop.component.css'],
  providers: [SessionStorageService]
})
export class HomeShopComponent implements OnInit {

  constructor(private router: Router, private sstorage: SessionStorageService) { }

  ngOnInit() {
    this.sstorage.store("shopID", "sh1")
  }

  viewComplaints() {
    this.router.navigate(['customer']);
  }

  viewLogs() {
    this.router.navigate(['logs-shop']);
  }
  
  track() {
    this.router.navigate(['track']);
  }

  addViolations() {
    this.router.navigate(['violation-shop']);
  }

  invoke() {
    this.router.navigate(['invoke']);
  }

}
