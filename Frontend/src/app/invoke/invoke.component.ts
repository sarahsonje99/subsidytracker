import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { SessionStorageService } from 'ngx-webstorage';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-invoke',
  templateUrl: './invoke.component.html',
  styleUrls: ['./invoke.component.css'],
  providers: [SessionStorageService]
})
export class InvokeComponent implements OnInit {

  customerId = new FormControl('');
  quantity = new FormControl('');
  sackId = new FormControl('');

  status :any
  error  : any
  subsidy : any
  sack : any
  s: boolean;
  errorB: boolean

  constructor(private router: Router, private http: Http, private sstorage: SessionStorageService) { }

  ngOnInit() {
    this.s = false;
    this.errorB = false;
  }

  viewCustomer() {
    this.router.navigate(['customer']);
  }

  viewLogs() {
    this.router.navigate(['logs-shop']);
  }
  
  track() {
    this.router.navigate(['track']);
  }

  addViolations() {
    this.router.navigate(['violation-shop']);
  }

  logOut() {
    this.router.navigate(['home-shop']); 
  }
  invoke() {
    this.router.navigate(['invoke']);
  }

  submit() {
    this.http.post('http://localhost:3000/api/InvokeSubsidy', {
      "$class": "subsidytracker.network.InvokeSubsidy",
      "citizenId": this.customerId.value,//this.sstorage.retrieve('userID'),
      "shopId": this.sstorage.retrieve('shopID'),
      "invokedQuantity": this.quantity.value,
      "sackId": this.sackId.value,
      "transactionId": "",
      "timestamp": new Date()
    })
    .subscribe((data:any) => {
      console.log(data._body);
      let d = JSON.parse(JSON.parse(data._body));
      console.log(d);
      this.status = d['status'];
      this.subsidy = d['subsidyId'];
      this.sack = d['sackId'];
      console.log(this.status);
      if(this.status != "Successful") {
        this.s = false
        this.errorB = true;
        // alert("Unable to invoke subsidy!\nError: "+this.status);
      }
      else {
        this.s = true;
      }
    }, error => {
      console.error(error);
      this.error = error;
    });
  }

}
