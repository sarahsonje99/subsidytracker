import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-shops-qc',
  templateUrl: './shops-qc.component.html',
  styleUrls: ['./shops-qc.component.css']
})
export class ShopsQcComponent implements OnInit {

  shops = []

  constructor(private router: Router, private http: Http) { }

  ngOnInit() {
  }

  viewShop() {
    this.router.navigate(['shop-qc']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  check() {
    this.router.navigate(['quality-check']);
  }

  logOut() {
    this.router.navigate(['home-qc']);
  }
}
