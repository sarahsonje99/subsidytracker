import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopsQcComponent } from './shops-qc.component';

describe('ShopsQcComponent', () => {
  let component: ShopsQcComponent;
  let fixture: ComponentFixture<ShopsQcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopsQcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopsQcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
