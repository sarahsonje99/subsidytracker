import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fps',
  templateUrl: './fps.component.html',
  styleUrls: ['./fps.component.css']
})
export class FpsComponent implements OnInit {

  shops=[]

  constructor(private http: Http, private router: Router) { }

  ngOnInit() {
    this.http.get('http://localhost:3000/api/Shop')
    .subscribe((data:any)=> {
      this.shops = JSON.parse(data._body);
      console.log(this.shops);
    })
  }

  viewFPS() {
    this.router.navigate(['fps']);
  }

  viewLogs() {
    this.router.navigate(['logs-gov']);
  }
  
  send() {
    this.router.navigate(['send']);
  }

  viewComplaints() {
    this.router.navigate(['view-violation']);
  }

  addSubsidy() {
    this.router.navigate(['add-subsidy']);
  }

  logOut() {
    this.router.navigate(['home-gov']);
  }

}
